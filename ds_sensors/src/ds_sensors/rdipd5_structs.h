/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_SENSORS_RDIPD5_STRUCTS
#define DS_SENSORS_RDIPD5_STRUCTS

namespace ds_sensors
{
namespace rdipd5_structs
{

struct dataframe
{
  uint16_t headerid;
  uint16_t bytes;
  uint8_t system_config;
  int16_t velocity[4]; // bottom vel
  uint16_t range[4]; // beams range to bottoms
  uint8_t bottom_status;
  int16_t layer_velocity[4]; // vel relative to water layer
  uint16_t ref_layer_start;
  uint16_t ref_layer_end;
  uint8_t ref_layer_status;
  uint8_t tofp_hour;
  uint8_t tofp_minute;
  uint8_t tofp_second;
  uint8_t tofp_hundreths;
  uint16_t bit_results;
  uint16_t sound_vel;
  uint16_t temperature;
  uint8_t salinity;
  uint16_t depth;
  uint16_t pitch;
  uint16_t roll;
  uint16_t heading;
  uint32_t bt_distance_east;
  uint32_t bt_distance_north;
  uint32_t bt_distance_up;
  uint32_t bt_distance_error;
  uint32_t ref_distance_east;
  uint32_t ref_distance_north;
  uint32_t ref_distance_up;
  uint32_t ref_distance_error;
  uint16_t checksum;
} __attribute__((packed));


}  // rdipd5_structs

}  // ds_sensors

#endif  // DS_SENSORS_RDIPD5_STRUCTS
