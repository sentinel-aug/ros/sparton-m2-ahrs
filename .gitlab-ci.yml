variables:
  ROS_VERSION: melodic

image: ros:${ROS_VERSION}-ros-base

stages:
  - Test
  - Build images
  - Build image manifests
  - Release

# roslint:
#   stage: Test
#   script:
#     - apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
#     - apt-get update
#     - apt-get install ros-${ROS_VERSION}-roslint
#     - source /opt/ros/${ROS_VERSION}/setup.bash
#     - mkdir -p /catkin_ws/src
#     - ln -s $(pwd) /catkin_ws/src/${CI_PROJECT_NAME}
#     - cd /catkin_ws/src
#     - catkin_init_workspace
#     - cd ..
#     - catkin_make
#     - catkin_make roslint
#   needs: []

Check if latest release:
  stage: Build images
  image: alpine
  script:
    - apk add git
    - touch gitlab-ci.env
    - |
      if [ "$CI_COMMIT_TAG" = "$(git tag -l "v*" | grep -v "-" | sort -Vr | head -1)" ]; then
        echo "TAG_AS_LATEST=yes" > gitlab-ci.env
      fi
  artifacts:
    reports:
      dotenv: gitlab-ci.env
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*(-.*)?$/'

.Build arch image - kaniko:
  stage: Build images
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    IMAGE_NAME: $CI_PROJECT_NAME
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor "--context=$CI_PROJECT_DIR" "--dockerfile=$DOCKERFILE" --cache=true --cache-repo="$CI_REGISTRY_IMAGE/kaniko/$IMAGE_NAME" --destination "$CI_REGISTRY_IMAGE/tmp/$IMAGE_NAME-${ARCH?}:$CI_PIPELINE_IID"

Build sparton-m2-ahrs image - amd64:
  extends: .Build arch image - kaniko
  variables:
    ARCH: amd64
    DOCKERFILE: docker/Dockerfile.sparton_m2_ahrs
    IMAGE_NAME: sparton-m2-ahrs

Build sparton-m2-ahrs image - arm64:
  extends: .Build arch image - kaniko
  variables:
    ARCH: arm64
    DOCKERFILE: docker/Dockerfile.sparton_m2_ahrs
    IMAGE_NAME: sparton-m2-ahrs
  tags:
    - arm64

.Build manifest:
  stage: Build image manifests
  image: alpine:latest
  script:
    - apk add curl ca-certificates
    - curl -fsSL https://github.com/estesp/manifest-tool/releases/download/v1.0.3/manifest-tool-linux-amd64 > /tmp/manifest-tool
    - chmod +x /tmp/manifest-tool
    - mv /tmp/manifest-tool /usr/local/bin/manifest-tool
    - |
      manifest-tool --username "$CI_REGISTRY_USER" --password "$CI_REGISTRY_PASSWORD" \
                    push from-args \
                    --ignore-missing \
                    --platforms linux/amd64,linux/arm64 \
                    --template $CI_REGISTRY_IMAGE/tmp/$IMAGE_NAME-ARCH:$CI_PIPELINE_IID \
                    --target $CI_REGISTRY_IMAGE/$IMAGE_NAME:$TAG

Build sparton-m2-ahrs sha manifest:
  extends: .Build manifest
  variables:
    IMAGE_NAME: sparton-m2-ahrs
    TAG: $CI_COMMIT_SHORT_SHA
  needs:
    - Build sparton-m2-ahrs image - amd64
    - Build sparton-m2-ahrs image - arm64

Build sparton-m2-ahrs ref manifest:
  extends: .Build manifest
  variables:
    IMAGE_NAME: sparton-m2-ahrs
    TAG: $CI_COMMIT_REF_NAME
  needs:
    - Build sparton-m2-ahrs image - amd64
    - Build sparton-m2-ahrs image - arm64

Build sparton-m2-ahrs release manifest:
  extends: .Build manifest
  variables:
    IMAGE_NAME: sparton-m2-ahrs
    TAG: $CI_COMMIT_TAG
  needs:
    - Build sparton-m2-ahrs image - amd64
    - Build sparton-m2-ahrs image - arm64
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*(-.*)?$/'

Build sparton-m2-ahrs latest manifest:
  extends: .Build manifest
  variables:
    IMAGE_NAME: sparton-m2-ahrs
    TAG: latest
  needs:
    - Check if latest release
    - Build sparton-m2-ahrs image - amd64
    - Build sparton-m2-ahrs image - arm64
  rules:
    - if: '($CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*(-.*)?$/) && ($TAG_AS_LATEST == "yes")'

Release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+(\.[0-9]+)*(-.*)?$/'
  script:
    - "true"
  release:
    name: 'Sparton M2 AHRS $CI_COMMIT_TAG'
    description: 'Sparton M2 AHRS ROS package $CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
