# Sparton M2 AHRS

Repository containing ROS driver and other relevant code for using the Sparton M2 IMU on a vehicle such as the Slocum Glider.

## To Run (through Docker)
Navigate to sparton-m2-ahrs directory
Run the following commands:
+ `docker_compose build`
+ `docker_compose up`

To see rostopics, open up a separate terminal and run following commands:
+ `docker-compose exec sparton_m2_ahrs /ros_entrypointa.sh bash` (opens up terminal inside container)
+ `rostopic list`

## To Run (natively)
Make sure your workspace is built and sourced
+ `cd your_catkin_ws`
+ `catkin_make`
+ `source devel/setup.bash`

Run launch file
+ `roslaunch ds_sensors spartonm2.launch serial_port:="/dev/ttyUSB0"`


## A note on serial ports
+ `docker.compose.yml` will look for an environment variable defined 
`SPARTON_SERIAL_PORT` otherwise it will default to `/dev/ttyUSB0`
+ The `spartonm2.launch` file looks for the arg `serial_port` for the location of the sensor (note: defaults to `/dev/ttyUSB0`)
+ To look up USBs (can be helpful to run this before and after plugging in device to know which is which):
`ls /dev/tty*`

## A note on Docker
To use the pre-built image. Navigate to Gitlab Packages&Registry --> Container Registry
The url can then be used in the following ways:
+ `docker pull url`
+ listed in the `docker-compose` file as the image

## TODO
+ Build images and test on PI 4 
